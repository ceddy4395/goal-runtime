GOAL Runtime
========

This project contains the GOAL runtime.
The runtime currently includes a GOAL agent(test) interpreter, MAS manager and Platform manager

Usage
=====
java -jar runtime-X.Y.Z-jar-with-dependencies.jar -h


Dependency Information
=============

```
<repository>
	<id>goalhub-mvn-repo</id>
	<url>https://raw.github.com/goalhub/mvn-repo/master</url>
</repository>

```

```
<dependency>
	<groupId>com.github.goalhub.runtime</groupId>
	<artifactId>runtime</artifactId>
	<version>X.Y.Z</version>
</dependency>
```	

Link to more information about GOAL
===================================
See: http://ii.tudelft.nl/trac/goal