/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package goal.tools.test;

import goal.preferences.CorePreferences;
import goal.tools.TestResultInspector;
import goal.tools.TestRun;
import goal.tools.errorhandling.exceptions.GOALRunFailedException;
import goal.tools.test.result.TestProgramResult;
import goal.tools.test.result.TestResultFormatter;
import languageTools.analyzer.FileRegistry;
import languageTools.analyzer.test.TestValidator;
import languageTools.program.test.TestProgram;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class AbstractTest {
	private static boolean previous;
	protected TestValidator visitor;

	@Before
	public void start() {
		// Loggers.addConsoleLogger();
		previous = CorePreferences.getAbortOnTestFailure();
		CorePreferences.setAbortOnTestFailure(true);
	}

	@After
	public void end() {
		CorePreferences.setAbortOnTestFailure(previous);
		// Loggers.removeConsoleLogger();
	}

	protected static void assertPassedAndPrint(TestProgramResult results) {
		TestResultFormatter formatter = new TestResultFormatter();
		System.out.println(formatter.visit(results));
		assertTrue(results.isPassed());
	}

	protected static void assertFailedAndPrint(TestProgramResult results) {
		TestResultFormatter formatter = new TestResultFormatter();
		System.out.println(formatter.visit(results));
		assertFalse(results.isPassed());
	}

	public TestProgram setup(String path) throws Exception {
		this.visitor = new TestValidator(path, new FileRegistry());
		this.visitor.validate();
		TestProgram program = this.visitor.getProgram();
		if (program != null && program.isValid()) {
			return program;
		} else {
			System.out.println(this.visitor.getSyntaxErrors());
			System.out.println(this.visitor.getErrors());
			throw new IOException();
		}
	}

	protected TestProgramResult runTest(String testFileName) throws Exception {
		TestProgram testProgram;
		try {
			testProgram = setup(testFileName);
		} catch (IOException e) {
			throw new GOALRunFailedException("error while reading test file " + testFileName, e);
		}

		assertNotNull(testProgram);

		TestRun testRun = new TestRun(testProgram, false);
		testRun.setDebuggerOutput(true);
		TestResultInspector inspector = new TestResultInspector(testProgram);
		testRun.setResultInspector(inspector);
		testRun.run(true);

		return inspector.getResults();
	}

	@Test
    public void testPercentage100() {
	    TestProgram testProgram = null;
	    try{
	        testProgram = setup("src/test/resources/goal/tools/test/counter/CountsTo100.test2g");
        } catch (Exception e) {
	        System.out.println("Unable to setup testPercentage100 test program");
            e.printStackTrace();
        }

        assertNotNull(testProgram);
        TestRun testRun = null;
        try {
            testRun = new TestRun(testProgram, false);
            testRun.run(false);
        } catch (GOALRunFailedException e) {
            e.printStackTrace();
        }
        assertTrue(testRun.calculatePercentage() == 100);

    }

    @Test
    public void testPercentage0() {
        TestProgram testProgram = null;
        try{
            testProgram = setup("src/test/resources/goal/tools/test/correctFailingLTL.test2g");
        } catch (Exception e) {
            System.out.println("Unable to setup testPercentage0 test program");
            e.printStackTrace();
        }

        assertNotNull(testProgram);
        TestRun testRun = null;
        try {
            testRun = new TestRun(testProgram, false);
            testRun.run(false);
        } catch (GOALRunFailedException e) {
            e.printStackTrace();
        }
        assertTrue(testRun.calculatePercentage() == 0);

    }

    @Test
    public void testPercentage50() {
        TestProgram testProgram = null;
        try{
            testProgram = setup("src/test/resources/goal/tools/test/example/blocksworld/failedtest/newOperators.test2g");
        } catch (Exception e) {
            System.out.println("Unable to setup testPercentage50 test program");
            e.printStackTrace();
        }

        assertNotNull(testProgram);
        TestRun testRun = null;
        try {
            testRun = new TestRun(testProgram, false);
            testRun.run(false);
        } catch (GOALRunFailedException e) {
            e.printStackTrace();
        }
        assertTrue(testRun.calculatePercentage() == 0);

    }
}