package goal.tools.test.example.blocksworld;

import goal.tools.test.AbstractTest;
import goal.tools.test.result.TestProgramResult;

public class OwlExampleTest extends AbstractTest {
	// @Test FIXME: takes very long
	public void testSimpleBlocksWorld() throws Exception {
		TestProgramResult results = runTest(
				"src/test/resources/goal/tools/test/example/blocksworld/owl_simple/blocksworld.test2g");
		assertPassedAndPrint(results);
	}
}
