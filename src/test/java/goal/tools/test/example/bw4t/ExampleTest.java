/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package goal.tools.test.example.bw4t;

import org.junit.Ignore;
import org.junit.Test;

import goal.tools.test.AbstractTest;
import goal.tools.test.result.TestProgramResult;

/**
 * Do some testing of BW4T. To run this test, you must have the server running
 * before starting the test. #2955 the idea of this test is that we test the
 * testframework itself.
 */
@SuppressWarnings("javadoc")
public class ExampleTest extends AbstractTest {

	@Ignore("We should make sure that the server is running.")
	@Test
	public void testBW4TExplorer() throws Exception {
		TestProgramResult results = runTest("src/test/resources/goal/tools/test/example/bw4t/bw4texplore.test2g");
		assertPassedAndPrint(results);
	}

	@Ignore("We should make sure that the server is running.")
	@Test
	public void testBW4TFinder() throws Exception {
		TestProgramResult results = runTest("src/test/resources/goal/tools/test/example/bw4t/bw4tfind.test2g");
		assertPassedAndPrint(results);
	}
}
