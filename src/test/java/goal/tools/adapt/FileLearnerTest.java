/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package goal.tools.adapt;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Before;

import eis.iilang.Action;
import eis.iilang.Percept;
import goal.core.agent.Agent;
import goal.core.agent.AgentRegistry;
import goal.core.agent.EnvironmentCapabilities;
import goal.core.agent.GOALInterpreter;
import goal.core.agent.LoggingCapabilities;
import goal.core.agent.NoLoggingCapabilities;
import goal.tools.debugger.NOPDebugger;
import krTools.KRInterface;
import languageTools.analyzer.FileRegistry;
import languageTools.analyzer.mas.MASValidator;
import languageTools.program.agent.AgentId;
import languageTools.program.mas.MASProgram;

public class FileLearnerTest {
	private class DummyEnvironment implements EnvironmentCapabilities {
		@Override
		public Double getReward() {
			return 1.0;
		}

		@Override
		public void performAction(Action action) {
			System.out.println(action);
		}

		@Override
		public Set<Percept> getPercepts() {
			return new HashSet<>(0);
		}

		@Override
		public void dispose() {
		}
	}

	private Agent<GOALInterpreter<NOPDebugger>> agent;
	private GOALInterpreter<NOPDebugger> controller;
	private KRInterface language;

	@Before
	public void setUp() throws Exception {
		AgentId id = new AgentId("TestAgent");
		String filename = "src/test/resources/goal/tools/adapt/adapt.mas2g";

		FileRegistry registry = new FileRegistry();
		MASValidator validator = new MASValidator(filename, registry);
		validator.validate();
		MASProgram program = validator.getProgram();
		EnvironmentCapabilities environmentCapabilities = new DummyEnvironment();
		LoggingCapabilities loggingCapabilities = new NoLoggingCapabilities();

		NOPDebugger debugger = new NOPDebugger(id);
		Learner learner = new FileLearner(id.getName(), program.getAgentDefinition(id.getName()));
		ExecutorService pool = Executors.newFixedThreadPool(1);
		AgentRegistry<GOALInterpreter<NOPDebugger>> agents = new AgentRegistry<>(loggingCapabilities, pool);

		this.controller = new GOALInterpreter<>(program.getAgentDefinition(id.getName()), agents, debugger, learner);
		this.agent = new Agent<>(id, environmentCapabilities, loggingCapabilities, this.controller, pool, 0);
		agents.register(this.agent);
	}

	// @Test FIXME
	public void testStart() throws Exception {
		this.controller.run();
		assertTrue(this.controller.isRunning());
		this.controller.awaitTermination(); // TODO: timeout?!
		assertFalse(this.controller.isRunning());
	}
}
