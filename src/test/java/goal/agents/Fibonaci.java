/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package goal.agents;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Tests whether running the fibonaci agent results in inserting result(ok) in
 * the belief base of the agent. Tests the basic functionality used in the test
 * runs we use.
 */
public class Fibonaci extends ProgramTest {
	/**
	 * Tests whether Fibonaci agent computes Fibonaci number 24 correctly.
	 */
	@Test
	public void fibonaciTest() throws Exception {
		assertEquals("46368", runAgent("src/test/resources/goal/agents/fibonaci.mas2g", "bel( fib(X) )"));
	}
}