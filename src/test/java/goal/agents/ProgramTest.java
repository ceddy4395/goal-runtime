/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package goal.agents;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import events.NoEventGenerator;
import goal.core.agent.AbstractAgentFactory;
import goal.core.agent.Agent;
import goal.core.agent.AgentFactory;
import goal.core.agent.GOALInterpreter;
import goal.tools.ResultInspector;
import goal.tools.SingleRun;
import goal.tools.adapt.Learner;
import goal.tools.debugger.Debugger;
import goal.tools.debugger.NOPDebugger;
import goal.tools.eclipse.QueryTool;
import goal.tools.errorhandling.exceptions.GOALLaunchFailureException;
import krTools.language.Substitution;
import krTools.language.Term;
import krTools.language.Var;
import languageTools.program.agent.Module.FocusMethod;
import languageTools.program.agent.msc.MentalStateCondition;
import languageTools.program.mas.AgentDefinition;
import mentalState.MentalStateWithEvents;
import mentalState.error.MSTDatabaseException;
import mentalState.error.MSTQueryException;
import mentalState.executors.MentalStateConditionExecutor;

/**
 * General utility functions for running tests using {@link SingleRun}
 * <p>
 * Basically this assumes that there is a GOAL mas2g program. There must be
 * exactly 1 agent in the MAS. There can be an environment.
 * </p>
 * <p>
 * The program runs the MAS till the agent is dead. This requires the program to
 * set up such that the main module of all agents exits when they are done with
 * the test.
 * </p>
 * <p>
 * Then the belief base of the agent is queried for @ code result(X)}. The known
 * states are {@code result(failure)} and {@code result(ok)}. If one of these is
 * found, {@link RunResult#OK} or {@link RunResult#FAILURE} is returned.
 * Otherwise {@link RunResult#UNKNOWN} is returned. Also errors may be thrown
 * depending on the case.
 * </p>
 */
public class ProgramTest {
	public enum RunResult {
		OK, FAILURE, UNKNOWN;

		public static RunResult get(String value) {
			try {
				return RunResult.valueOf(value.toUpperCase());
			} catch (IllegalArgumentException e) {
				return RunResult.UNKNOWN;
			}
		}
	}

	private Agent<GOALInterpreter<Debugger>> buildAgent(AgentDefinition agent) throws Exception {
		ExecutorService pool = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 1L, TimeUnit.SECONDS,
				new SynchronousQueue<Runnable>());
		return new SimpleAgentFactory(pool, 0).build(agent, agent.getName(), null);
	}

	/**
	 * Do a single run of given MAS file with a single agent. Checks after the
	 * run why the agent terminated. The agent should hold the belief "ok" or
	 * "failure", and that is the reason to return. If neither is holding, we
	 * return {@link RunResult#UNKNOWN}.
	 *
	 * @param masFile
	 */
	protected RunResult runAgent(String masFile) throws Exception {
		SingleRun run = loadMAS(masFile, 0);
		AgentDefinition agentDf = run.getProgram()
				.getAgentDefinition(run.getProgram().getAgentNames().iterator().next());
		Agent<GOALInterpreter<Debugger>> agent = buildAgent(agentDf);
		try {
			agent.start();
			agent.awaitTermination();
			return inspectResult(agent);
		} finally {
			agent.dispose(true);
		}

	}

	/**
	 * load masfile to run with given time-out
	 *
	 * @param masFile
	 *            the file to load and run
	 * @param timeout
	 *            number of seconds for running
	 * @return {@link SingleRun}.
	 */
	private SingleRun loadMAS(String masFile, int timeout) throws Exception {
		File file = new File(masFile);
		SingleRun run = new SingleRun(file) {
			@Override
			protected AgentFactory<Debugger, GOALInterpreter<Debugger>> buildAgentFactory()
					throws GOALLaunchFailureException {
				try {
					return new SimpleAgentFactory(this.pool, this.timeout);
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		};
		run.setDebuggerOutput(true);
		run.setTimeOut(timeout);
		if (!run.getErrors().isEmpty()) {
			throw new Exception("Found parsing errors: " + run.getErrors());
		}
		return run;
	}

	/**
	 * Run all the agents. Wait till all have terminated. Collect the results
	 *
	 * @param masFile
	 *            the mas file to run
	 * @return map with <AgentName, RunResult>
	 */
	protected Map<String, RunResult> runAgents(String masFile, int timeout) throws Exception {
		// the queue where the result is stored
		final BlockingQueue<Map<String, RunResult>> resultsQueue = new LinkedBlockingQueue<>();

		SingleRun run = loadMAS(masFile, timeout);
		run.setResultInspector(new ResultInspector<GOALInterpreter<Debugger>>() {
			@Override
			public void handleResult(Collection<Agent<GOALInterpreter<Debugger>>> agents) {
				final Map<String, RunResult> results = new HashMap<>();
				for (Agent<GOALInterpreter<Debugger>> agent : agents) {
					results.put(agent.getId().getName(), inspectResult(agent));
				}
				resultsQueue.add(results);
			}
		});
		run.run(true);

		return resultsQueue.take();
	}

	/**
	 * Do a single run of given MAS file with a single agent. Checks after the
	 * run whether the given mental state condition holds.
	 *
	 * @param masFile
	 *            A MAS file.
	 * @param msc
	 *            A mental state condition with exactly one variable (to collect
	 *            result).
	 * @return String representation of the collected result.
	 */
	protected String runAgent(String masFile, String msc) throws Exception {
		File file = new File(masFile);
		SingleRun run = new SingleRun(file);
		if (!run.getErrors().isEmpty()) {
			throw new GOALLaunchFailureException("Found parsing errors: " + run.getErrors());
		}
		AgentDefinition agentDf = run.getProgram()
				.getAgentDefinition(run.getProgram().getAgentNames().iterator().next());
		Agent<GOALInterpreter<Debugger>> agent = buildAgent(agentDf);
		try {
			agent.start();
			agent.awaitTermination();
			return inspectResult(agent, msc);
		} finally {
			agent.dispose(true);
		}

	}

	protected RunResult inspectResult(Agent<GOALInterpreter<Debugger>> agent) {
		return RunResult.get(inspectResult(agent, "bel(result(X))"));
	}

	protected String inspectResult(Agent<GOALInterpreter<Debugger>> agent, String msc) {
		QueryTool buildQuery = new QueryTool(agent);
		MentalStateCondition mentalStateCondition;
		try {
			mentalStateCondition = buildQuery.parseMSC(msc);
		} catch (Exception e) {
			throw new IllegalStateException("Unexpected exception whilst building MSC", e);
		}

		MentalStateWithEvents mentalState = agent.getController().getRunState().getMentalState();
		Set<Substitution> res;
		try {
			Substitution empty = mentalState.getOwner().getKRInterface().getSubstitution(null);
			MentalStateConditionExecutor msce = new MentalStateConditionExecutor(mentalStateCondition, empty);
			res = msce.evaluate(mentalState, FocusMethod.NONE, new NoEventGenerator()).getAnswers();
		} catch (MSTQueryException | MSTDatabaseException e) {
			throw new IllegalStateException("evaluation of mental state " + mentalState + " failed", e);
		}

		// there should be exactly 1 substi.
		if (res.size() < 1) {
			throw new IllegalStateException("program failed: it did not set a result");
		} else if (res.size() > 1) {
			throw new IllegalStateException("program failed: it set multiple results (ony 1 allowed)");
		} else {
			// and it should hold exactly 1 variable, our variable "X". Get its
			// value
			Substitution substitution = ((Substitution) res.toArray()[0]);
			List<Var> variables = substitution.getVariables();
			if (variables.size() < 1) {
				throw new IllegalStateException("query failed: it did not set a result");
			} else if (variables.size() > 1) {
				throw new IllegalStateException("query failed: it set multiple results (ony 1 allowed)");
			} else {
				Var var = (Var) variables.toArray()[0];
				Term value = substitution.get(var);
				return value.toString();
			}
		}
	}

	/**
	 * Abstract base for build agents without messaging or an environment.
	 * Subclasses can provide different Messaging- and EnvironmentCapabilities,
	 * Debuggers, Learners and Controllers. This can be done by overriding or
	 * implementing the proper methods.
	 *
	 * During the construction the class fields will be initialized to assist
	 * the creation of different classes.
	 */
	private class SimpleAgentFactory extends AbstractAgentFactory<Debugger, GOALInterpreter<Debugger>> {
		public SimpleAgentFactory(ExecutorService pool, long timeout) throws Exception {
			super(pool, timeout);
		}

		@Override
		protected Debugger provideDebugger() {
			return new NOPDebugger(getAgentId());
		}

		@Override
		protected GOALInterpreter<Debugger> provideController(Debugger debugger, Learner learner) {
			GOALInterpreter<Debugger> controller = new GOALInterpreter<>(getAgentDf(), getRegistry(), debugger,
					learner);
			// need to run a query after the MAS terminates
			controller.keepDataOnTermination();
			return controller;
		}
	}
}