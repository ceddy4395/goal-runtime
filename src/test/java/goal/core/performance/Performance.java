/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package goal.core.performance;

import goal.tools.Run;

/**
 * Performance tests.
 */
public class Performance {
	// @Test
	public void testTokenRingLocal() throws Exception {
		Run.run("src/test/resources/goal/agents/tokenring2/tokenRing.mas2g");
	}

	// FIXME @Test
	public void testTokenRingRmi() throws Exception {
		Run.run("src/test/resources/goal/agents/tokenring/token.mas2g", "--messagingtype", "rmi");
	}

	// FIXME @Test
	public void testChameneos() throws Exception {
		Run.run("src/test/resources/goal/core/performance/chameneos/chameneos.mas2g");
	}

	// FIXME @Test
	public void testChameneosOnRMI() throws Exception {
		Run.run("src/test/resources/goal/core/performance/chameneos/chameneos.mas2g", "--messagingtype", "rmi");
	}
}