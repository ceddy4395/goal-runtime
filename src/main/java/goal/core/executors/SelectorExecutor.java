package goal.core.executors;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import goal.core.agent.AgentRegistry;
import goal.core.executors.actions.SendActionExecutor;
import goal.tools.errorhandling.Warning;
import krTools.language.Term;
import languageTools.program.agent.AgentId;
import languageTools.program.agent.selector.Selector;

/**
 * Extends selector from mental state project (
 * {@link mentalState.executors.SelectorExecutor}) with a method to evaluate a
 * selector using an agent registry, which is used by the send action (
 * {@link SendActionExecutor}).
 */
public class SelectorExecutor extends mentalState.executors.SelectorExecutor {

	public SelectorExecutor(Selector selector) {
		super(selector);
	}

	public List<AgentId> evaluate(AgentId aid, AgentRegistry<?> registry) {
		List<AgentId> agents = new LinkedList<>();
		switch (getSelector().getType()) {
		default:
		case THIS:
		case SELF:
			agents.add(aid);
			break;
		case ALL: // ALL is inclusive; keep own aid.
			agents.addAll(registry.getRegisteredAgents());
			break;
		case ALLOTHER: // ALLOTHER is exclusive; remove own aid.
			List<AgentId> registered1 = new ArrayList<>(registry.getRegisteredAgents());
			registered1.remove(aid);
			if (registered1.isEmpty()) {
				new Warning("no other agent(s) to send messages to.");
			} else {
				agents.addAll(registered1);
			}
			break;
		case SOME: // SOME is inclusive; keep own aid.
			List<AgentId> registered2 = new ArrayList<>(registry.getRegisteredAgents());
			int pick1 = new Random().nextInt(registered2.size());
			agents.add(registered2.get(pick1));
			break;
		case SOMEOTHER: // SOMEOTHER is exclusive; remove own aid.
			List<AgentId> registered3 = new ArrayList<>(registry.getRegisteredAgents());
			registered3.remove(aid);
			if (registered3.isEmpty()) {
				new Warning("no other agent to send messages to.");
			} else {
				int pick2 = new Random().nextInt(registered3.size());
				agents.add(registered3.get(pick2));
			}
			break;
		case PARAMETERLIST:
			// Cannot be a variable selector (won't check this here because of
			// efficiency), which should have been checked by validation
			// already. Should be a list of closed terms; resolve.
			for (Term term : getSelector().getParameters()) {
				String name = term.toString();
				// Strip single or double quotes in term's name, if any.
				if (name.startsWith("\'") || name.startsWith("\"")) {
					name = name.substring(1, name.length() - 1);
				}
				// Try to find the referenced agent or channel...
				AgentId agentId = new AgentId(name);
				if (registry.isRegistered(agentId)) {
					agents.add(agentId);
				} else if (registry.isChannel(name)) {
					agents.addAll(registry.getSubscribers(name));
				} else {
					new Warning("reference to non-existing agent or channel '" + term + "' in selector '"
							+ getSelector() + "'.");
				}
			}
			break;
		}

		return agents;
	}

}