/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package goal.core.executors.modules;

import java.util.Collections;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import events.Channel;
import events.ExecutionEventGeneratorInterface;
import goal.core.executors.stack.CallStack;
import goal.core.executors.stack.RuleStackExecutor;
import goal.core.runtime.service.agent.Result;
import goal.core.runtime.service.agent.RunState;
import goal.tools.errorhandling.exceptions.GOALActionFailedException;
import krTools.language.DatabaseFormula;
import krTools.language.Query;
import krTools.language.Substitution;
import krTools.language.Update;
import languageTools.program.agent.Module;
import languageTools.program.agent.Module.RuleEvaluationOrder;
import languageTools.program.agent.rules.Rule;
import languageTools.program.agent.selector.Selector;
import languageTools.program.agent.selector.Selector.SelectorType;
import mentalState.GoalBase;
import mentalState.MentalStateWithEvents;
import mentalState.error.MSTDatabaseException;
import mentalState.error.MSTException;
import mentalState.error.MSTQueryException;

/**
 * Executor for a regular {@link Module}. Keeps its own stack of
 * {@link RuleStackExecutor}s as initially determined by {@link #pushed()}, but
 * then modified by {@link #popped()}, i.e. whilst the module is running. Not
 * all rules in this stack will be pushed to the {@link CallStack} (and thus
 * executed), as the module might terminate at any point in the execution. If
 * there is an event module, it will be 'automatically' put on the stack by this
 * executor when a module has finished evaluation all rules but none applied.
 */
public class LinearModuleExecutor extends ModuleExecutor {
	/**
	 * Have we prepared the mental state for the module yet? The first call to
	 * {@link #pushed()} will put this to true.
	 */
	private boolean prepared;
	/**
	 * The rules to execute as initially determined by {@link #pushed()}, but
	 * then modified by {@link #popped()}, i.e. whilst the module is running.
	 */
	private Deque<RuleStackExecutor> rules;
	/**
	 * The last result of an execute-call.
	 */
	private Result result;
	/**
	 * A possible exception (instead of a result)
	 */
	private GOALActionFailedException failure;

	/**
	 * Create an executor for a {@link Module}.
	 *
	 * @param parent
	 *            The {@link CallStack} that we are working in.
	 * @param runstate
	 *            The {@link RunState} (i.e. agent) that we are working for.
	 * @param module
	 *            The {@link Module} that is to be executed.
	 * @param substitution
	 *            The {@link Substitution} to be used for instantiating
	 *            parameters of the module.
	 * @param defaultRuleOrder
	 *            the order in which the rules in this module are to be
	 *            evaulated if the module didn't specify an order.
	 */
	LinearModuleExecutor(CallStack parent, RunState runstate, Module module, Substitution substitution,
			RuleEvaluationOrder defaultRuleOrder) {
		super(parent, runstate, module, substitution, defaultRuleOrder);
	}

	/**
	 * loads this.rules with executors for all rules in this module.
	 */
	@SuppressWarnings("unchecked")
	private void setRules() {
		this.result = new Result();
		// Create all initial rule executors, and shuffle them if needed.
		this.rules = new LinkedList<>();
		Module module = getModule();
		for (Rule rule : module.getRules()) {
			RuleStackExecutor executor = (RuleStackExecutor) getExecutor(rule, getSubstitution());
			executor.setContext(module);
			this.rules.add(executor);
		}
		if (getRuleOrder() == RuleEvaluationOrder.RANDOM || getRuleOrder() == RuleEvaluationOrder.RANDOMALL) {
			Collections.shuffle((List<RuleStackExecutor>) this.rules);
		}
	}

	@Override
	public void popped() {
		if (this.failure != null) {
			return;
		}
		Module module = getModule();
		ExecutionEventGeneratorInterface generator = this.runstate.getEventGenerator();

		try {
			if (!this.prepared) {
				// First call; prepare!
				// Push (non-anonymous) modules that were just entered onto
				// stack that keeps track of modules that have been entered but
				// not yet exited again and initialize the mental state for the
				// module, i.e. initial beliefs/goals and possible focus.
				if (!module.isAnonymous()) {
					generator.event(Channel.MODULE_ENTRY, module, module.getDefinition(), "entered '%s' with %s.",
							module, getSubstitution());
				}
				this.runstate.enteredModule(module);
				prepareMentalState(); // sets this.prepared
				setRules();
			}
			// Check if we have just finished executing a rule;
			// if so we need to check if we need to exit the module.
			RuleStackExecutor previous = (getPrevious() instanceof RuleStackExecutor)
					? (RuleStackExecutor) getPrevious() : null;
			boolean exit = (previous == null) ? false : this.rules.isEmpty();
			boolean all = (getRuleOrder() == RuleEvaluationOrder.LINEARALL
					|| getRuleOrder() == RuleEvaluationOrder.RANDOMALL
					|| getRuleOrder() == RuleEvaluationOrder.LINEARALLRANDOM);
			if (previous != null) {
				this.result.merge(previous.getResult());
				exit = isModuleTerminated(exit || (!all && this.result.justPerformedAction()));
			}
			Result previousResult = this.result;
			boolean reset = this.rules.isEmpty() || (!all && this.result.justPerformedAction());
			// Clean up if we should exit the module
			if (exit) {
				this.rules.clear();
				// Remove focus on attention set if we exit module again.
				removeFocus();
				// Remove the module from the tack of modules that have been
				// entered and possibly update top level context in which we run
				this.runstate.exitModule(module);
				// Report the module exit on the module's debug channel.
				if (!module.isAnonymous()) {
					this.result.setModuleTerminated(true);
					generator.event(Channel.MODULE_EXIT, module, module.getDefinition(), "exited '%s'.", module);
				}
			} else {
				// Re-initialize the set of rules if we are
				// executing them all sequentially but have no more left OR if
				// the previous rule succeeded (for a linear module)
				if (reset) {
					setRules();
				}
				// Check whether we need to start a new cycle. We do so if we do
				// NOT exit this module, some action has been performed while
				// evaluating the module's rules, or a new percept or message
				// has arrived. We also need to be running within the main
				// module's context (never start a new cycle when running the
				// init/event or a module called from either of these modules).
				// Otherwise, put the module itself back on the stack,
				// and add the next rule to execute to it.
				if (!doEvent(reset, previousResult) && !this.rules.isEmpty()) {
					select(this);
					select(this.rules.remove());
				}
			}
		} catch (GOALActionFailedException e) {
			this.failure = e;
		}
	}

	private boolean doEvent(boolean reset, Result previousResult) throws GOALActionFailedException {
		// Check whether we need to start a new cycle. We do so if we do
		// NOT exit this module and are running within the main
		// module's context (never start a new cycle when running the
		// init/event or a module called from either of these modules).
		if (reset && this.runstate.isMainModuleRunning()) {
			this.runstate.startCycle(previousResult.justPerformedRealAction());
			Module event = this.runstate.getEventModule();
			if (event != null) {
				ModuleExecutor exec = ModuleExecutor.getModuleExecutor(this.parent, this.runstate, event,
						event.getKRInterface().getSubstitution(null), RuleEvaluationOrder.LINEARALL);
				select(this);
				select(exec);
				return true;
			}
		}
		return false;
	}

	@Override
	public Result getResult() throws GOALActionFailedException {
		if (this.failure == null) {
			return this.result;
		} else {
			throw this.failure;
		}
	}

	/**
	 * If the module has a focus option, then create a new attention set in line
	 * with the option specified. Then add beliefs and goals from module's
	 * beliefs and goals section to the agent's mental state.
	 *
	 * @param runState
	 *            The run state used to prepare the module's execution.
	 * @throws GOALActionFailedException
	 *             If inserting a belief or adopting a goal failed.
	 */
	private void prepareMentalState() throws GOALActionFailedException {
		// Create new attention set if module uses focus option.
		setFocus();

		Module module = getModule();
		MentalStateWithEvents mentalState = this.runstate.getMentalState();
		ExecutionEventGeneratorInterface generator = this.runstate.getEventGenerator();
		// FIXME: many conversions going on below?
		try {
			// Add beliefs specified in the beliefs section of this module.
			for (DatabaseFormula belief : module.getBeliefs()) {
				DatabaseFormula bel = belief.applySubst(getSubstitution());
				mentalState.insert(bel, generator, this.runstate.getId());
				generator.event(Channel.BB_UPDATES, bel.toQuery().toUpdate(), bel.getSourceInfo(),
						"'%s' has been inserted into the belief base '%s'.", bel.toQuery().toUpdate(),
						new Selector(SelectorType.THIS));
			}
			// Add goals specified in the goals section of this module.
			for (Query goal : module.getGoals()) {
				Update upd = goal.toUpdate().applySubst(getSubstitution());
				mentalState.adopt(upd, true, generator, this.runstate.getId());
				generator.event(Channel.GB_UPDATES, upd, upd.getSourceInfo(),
						"'%s' has been adopted into the goal base '%s'.", upd, mentalState.getAttentionSet().getName());
			}
		} catch (MSTException e) {
			throw new GOALActionFailedException("execution of module '" + module + "' failed.", e);
		}

		this.prepared = true;
	}

	/**
	 * Sets a new focus based on the focus method used.
	 *
	 * @param subst
	 * @param goal
	 * @return new {@link GoalBase} to use for the focus action. May be null if
	 *         no refocus is needed (reuse the existing {@link GoalBase}.
	 * @throws GOALActionFailedException
	 */
	private void setFocus() throws GOALActionFailedException {
		Module module = getModule();
		MentalStateWithEvents mentalState = this.runstate.getMentalState();
		ExecutionEventGeneratorInterface generator = this.runstate.getEventGenerator();
		try {
			String attentionSet = mentalState.setFocus(module.toString(), getFocus(), module.getFocusMethod());
			if (attentionSet != null) {
				generator.event(Channel.GB_CHANGES, attentionSet, null, "focused to '%s'.", attentionSet);
				for (Update goal : mentalState.getAttentionSet().getUpdates()) {
					generator.event(Channel.GB_UPDATES, goal, goal.getSourceInfo(),
							"'%s' has been adopted into the goal base '%s'.", goal, attentionSet);
				}
			}
		} catch (MSTDatabaseException | MSTQueryException e) {
			throw new GOALActionFailedException("failed to set focus for '" + module.getName() + "'.", e);
		}
	}

	/**
	 * Defocus the module (if it was focused).
	 */
	private void removeFocus() {
		MentalStateWithEvents mentalState = this.runstate.getMentalState();
		if (mentalState.isFocussedOn(getModule().toString())) {
			ExecutionEventGeneratorInterface generator = this.runstate.getEventGenerator();
			try {
				String attentionSet = mentalState.getAttentionSet().getName();
				Set<Update> dropped = mentalState.getAttentionSet().getUpdates();
				mentalState.defocus();
				for (Update goal : dropped) {
					generator.event(Channel.GB_UPDATES, goal, goal.getSourceInfo(),
							"'%s' has been dropped from the goal base '%s'.", goal, attentionSet);
				}
				generator.event(Channel.GB_CHANGES, attentionSet, null, "dropped goalbase '%s'", attentionSet);
			} catch (MSTQueryException | MSTDatabaseException e) {
				this.failure = new GOALActionFailedException(
						"failed to defocus the attention of agent '" + this.runstate.getId() + "'.", e);
			}
		}
	}

	/**
	 * Evaluates whether the module should be terminated.
	 *
	 * @param runState
	 *            The run state used for evaluating the termination conditions.
	 * @return {@code true} if the module needs to be terminated; {@code false}
	 *         otherwise.
	 * @throws GOALActionFailedException
	 */
	private boolean isModuleTerminated(boolean noMoreRules) throws GOALActionFailedException {
		// Set exit flag if {@link ExitModuleAction} has been performed.
		boolean exit = this.result.isModuleTerminated();

		// Evaluate module's exit condition.
		if (noMoreRules) {
			switch (getModule().getExitCondition()) {
			case NOGOALS:
				try {
					exit |= !this.runstate.getMentalState().hasGoals();
				} catch (MSTDatabaseException | MSTQueryException e) {
					throw new GOALActionFailedException(
							"could not verify whether agent '" + this.runstate.getId() + "' has goals.", e);
				}
				break;
			case NOACTION:
				exit |= !this.result.justPerformedAction();
				break;
			case ALWAYS:
				exit |= true;
				break;
			default:
			case NEVER:
				// exit whenever module has been terminated (see above)
				break;
			}
		}
		exit |= !this.runstate.getParent().isRunning();
		return exit;
	}
}