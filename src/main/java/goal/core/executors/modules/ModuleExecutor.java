package goal.core.executors.modules;

import goal.core.executors.stack.CallStack;
import goal.core.executors.stack.StackExecutor;
import goal.core.runtime.service.agent.RunState;
import krTools.language.Substitution;
import languageTools.program.agent.Module;
import languageTools.program.agent.Module.RuleEvaluationOrder;
import languageTools.program.agent.msc.MentalStateCondition;

/**
 * Abstract class for module executors, and also factory for module executors.
 *
 * <p>
 * An action executor determines how to execute a {@link Module}.
 * <p>
 */
public abstract class ModuleExecutor extends StackExecutor {
	/**
	 * The module to be executed.
	 */
	private final Module module;
	/**
	 * Substitution to be used for instantiating parameters of the module.
	 */
	private final Substitution substitution;
	/**
	 * The goal to be focused on (if any).
	 */
	private MentalStateCondition focus;
	/**
	 * the order in which the rules in this module are to be evaluated if the
	 * module didn't specify an order
	 */
	private RuleEvaluationOrder ruleOrder;

	/**
	 * Create an executor for a {@link Module}.
	 *
	 * @param parent
	 *            The {@link CallStack} that we are working in.
	 * @param runstate
	 *            The {@link RunState} (i.e. agent) that we are working for.
	 * @param module
	 *            The {@link Module} that is to be executed.
	 * @param substitution
	 *            The {@link Substitution} to be used for instantiating
	 *            parameters of the module.
	 * @param ruleOrder
	 *            the default rule order, to be used if the module does not
	 *            specify a rule order. Must never be null.
	 */
	ModuleExecutor(CallStack parent, RunState runstate, Module module, Substitution substitution,
			RuleEvaluationOrder defaultRuleOrder) {
		super(parent, runstate);
		this.module = module;
		this.substitution = substitution;
		this.ruleOrder = module.getRuleEvaluationOrder();
		if (this.ruleOrder == null) {
			this.ruleOrder = defaultRuleOrder;
		}
	}

	/**
	 * @param focus
	 *            The goal to focus on (if any, can be null)
	 */
	public void setFocus(MentalStateCondition focus) {
		this.focus = focus;
	}

	/**
	 * @return The goal to focus on (if any, can be null)
	 */
	public MentalStateCondition getFocus() {
		return this.focus;
	}

	/**
	 * @return The module to be executed.
	 */
	public Module getModule() {
		return this.module;
	}

	/**
	 * @return The substitution that ...
	 */
	public Substitution getSubstitution() {
		return this.substitution;
	}

	/**
	 * @return the {@link RuleEvaluationOrder} of this module executor.
	 */
	public RuleEvaluationOrder getRuleOrder() {
		return this.ruleOrder;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " for " + getModule() + " with " + getSubstitution();
	}

	/**
	 * ModuleExecutor factory. Creates a module executor for the module.
	 *
	 * @param parent
	 *            The {@link CallStack} that we are working in.
	 * @param runstate
	 *            The {@link RunState} (i.e. agent) that we are working for.
	 * @param module
	 *            The {@link Module} to create an executor for.
	 * @param substitution
	 *            The {@link Substitution} to be used for instantiating
	 *            parameters of the module.
	 * @param defaultRuleOrder
	 *            the order in which the rules in this module are to be
	 *            evaluated if the module didn't specify an order.
	 * @return A module executor for the module. If the module requests specific
	 *         rule order, that order is used; otherwise the default rule order
	 *         is used.
	 */
	public static ModuleExecutor getModuleExecutor(CallStack parent, RunState runstate, Module module,
			Substitution substitution, RuleEvaluationOrder defaultRuleOrder) {
		// (add other rule orders like adaptive here)
		// TODO: perhaps also split in different executors
		RuleEvaluationOrder order = module.getRuleEvaluationOrder();
		if (order == null) {
			order = defaultRuleOrder;
		}
		return new LinearModuleExecutor(parent, runstate, module, substitution, order);
	}
}
