/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package goal.core.executors.actions;

import events.Channel;
import events.ExecutionEventGeneratorInterface;
import goal.core.runtime.service.agent.Result;
import goal.core.runtime.service.agent.RunState;
import goal.tools.errorhandling.exceptions.GOALActionFailedException;
import krTools.language.Substitution;
import krTools.language.Update;
import languageTools.program.agent.actions.AdoptAction;
import languageTools.program.agent.selector.Selector;
import languageTools.program.agent.selector.Selector.SelectorType;
import mentalState.MentalStateWithEvents;
import mentalState.error.MSTDatabaseException;
import mentalState.error.MSTQueryException;

/**
 * Executor for the adopt action.
 */
public class AdoptActionExecutor extends ActionExecutor {
	/**
	 * Executor for the adopt action.
	 *
	 * @param action
	 *            An adopt action.
	 * @param substitution
	 *            Substitution for instantiating parameters of the adopt action.
	 */
	AdoptActionExecutor(AdoptAction action, Substitution substitution) {
		super(action, substitution);
	}

	@Override
	public Result execute(RunState runState) throws GOALActionFailedException {
		AdoptAction adopt = ((AdoptAction) getAction());

		// Check selector.
		Selector selector = adopt.getSelector();
		if (selector.getType() != SelectorType.SELF && selector.getType() != SelectorType.THIS) {
			throw new GOALActionFailedException("only 'SELF' and 'THIS' are supported selectors for adopt actions.");
		}
		boolean topLevel = (adopt.getSelector().getType() == SelectorType.SELF);

		MentalStateWithEvents mentalState = runState.getMentalState();
		ExecutionEventGeneratorInterface generator = runState.getEventGenerator();
		Update upd = adopt.getUpdate().applySubst(getSourceSubstitution());
		try {
			mentalState.adopt(upd, !topLevel, generator);
			String base = topLevel ? mentalState.getAttentionStack().getFirst().getName()
					: mentalState.getAttentionStack().getLast().getName();
			generator.event(Channel.GB_UPDATES, upd, upd.getSourceInfo(),
					"'%s' has been adopted into the goal base '%s'.", upd, base);
			return new Result(getAction());
		} catch (MSTQueryException | MSTDatabaseException e) {
			throw new GOALActionFailedException(
					"failed to execute '" + adopt + "' with " + getSourceSubstitution() + ".", e);
		}
	}
}