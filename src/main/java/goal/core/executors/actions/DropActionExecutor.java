/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package goal.core.executors.actions;

import java.util.List;
import java.util.Map;

import events.Channel;
import events.ExecutionEventGeneratorInterface;
import goal.core.runtime.service.agent.Result;
import goal.core.runtime.service.agent.RunState;
import goal.tools.errorhandling.exceptions.GOALActionFailedException;
import krTools.language.Substitution;
import krTools.language.Update;
import languageTools.program.agent.actions.DropAction;
import languageTools.program.agent.selector.Selector;
import languageTools.program.agent.selector.Selector.SelectorType;
import mentalState.MentalStateWithEvents;
import mentalState.error.MSTDatabaseException;
import mentalState.error.MSTQueryException;

/**
 * Executor for the drop action.
 */
public class DropActionExecutor extends ActionExecutor {
	/**
	 * Executor for the drop action.
	 *
	 * @param action
	 *            A drop action.
	 * @param substitution
	 *            Substitution for instantiating parameters of the drop action.
	 */
	DropActionExecutor(DropAction action, Substitution substitution) {
		super(action, substitution);
	}

	@Override
	public Result execute(RunState runState) throws GOALActionFailedException {
		DropAction drop = (DropAction) getAction();

		// Check selector.
		Selector selector = drop.getSelector();
		if (selector.getType() != SelectorType.SELF && selector.getType() != SelectorType.THIS) {
			throw new GOALActionFailedException("only 'SELF' and 'THIS' are supported selectors for drop actions.");
		}

		MentalStateWithEvents mentalState = runState.getMentalState();
		ExecutionEventGeneratorInterface generator = runState.getEventGenerator();
		Update update = drop.getUpdate().applySubst(getSourceSubstitution());
		try {
			Map<String, List<Update>> droppedGoals = mentalState.drop(update, generator);
			for (String goalbase : droppedGoals.keySet()) {
				for (Update goal : droppedGoals.get(goalbase)) {
					generator.event(Channel.GB_UPDATES, goal, goal.getSourceInfo(),
							"'%s' has been dropped from the goal base '%s'.", goal, goalbase);
				}
			}
			return new Result(getAction());
		} catch (MSTQueryException | MSTDatabaseException e) {
			throw new GOALActionFailedException(
					"failed to execute '" + drop + "' with " + getSourceSubstitution() + ".", e);
		}
	}

}
