/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package goal.core.agent;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;

import events.Channel;
import events.ExecutionEventListener;
import goal.core.executors.modules.ModuleExecutor;
import goal.core.executors.stack.CallStack;
import goal.core.executors.stack.StackExecutor;
import goal.core.runtime.service.agent.RunState;
import goal.preferences.LoggingPreferences;
import goal.preferences.ProfilerPreferences;
import goal.tools.adapt.Learner;
import goal.tools.debugger.Debugger;
import goal.tools.debugger.DebuggerKilledException;
import goal.tools.debugger.NOPDebugger;
import goal.tools.debugger.SteppingDebugger;
import goal.tools.errorhandling.Warning;
import goal.tools.errorhandling.exceptions.GOALActionFailedException;
import goal.tools.errorhandling.exceptions.GOALLaunchFailureException;
import goal.tools.errorhandling.exceptions.GOALRuntimeErrorException;
import goal.tools.logging.InfoLog;
import goal.tools.profiler.InfoType;
import goal.tools.profiler.ProfileStatistic;
import goal.tools.profiler.Profiler;
import goal.util.datatable.ColumnType;
import goal.util.datatable.DataRow;
import goal.util.datatable.DataTable;
import krTools.parser.SourceInfo;
import languageTools.program.agent.Module;
import languageTools.program.agent.Module.RuleEvaluationOrder;
import languageTools.program.agent.actions.Action;
import languageTools.program.agent.actions.ActionCombo;
import languageTools.program.agent.actions.MentalAction;
import languageTools.program.mas.AgentDefinition;
import mentalState.error.MSTDatabaseException;
import mentalState.error.MSTQueryException;

/**
 * Agent controller (interpreter) for an agent ({@link AgentDefinition}).
 * <p>
 * The controller can be provided with a {@link Debugger} that will be called at
 * specific points during a run.
 * </p>
 *
 * A {@link Learner} will be consulted during the Adaptive sections of a
 * AgentDefinition.
 *
 * @param <DEBUGGER>
 *            class of the Debugger used by the interpreter.
 */
public class GOALInterpreter<DEBUGGER extends Debugger> extends Controller {
	/**
	 * The {@link RunState} of this agent. Records the current state of the
	 * interpreter in the GOAL Program.
	 */
	private RunState runState;
	/**
	 * Program ran by the interpreter.
	 */
	private final AgentDefinition agentDf;
	/**
	 * The agent registry.
	 */
	private final AgentRegistry<?> registry;
	/**
	 * Debugger used while running the interpreter.
	 */
	private final DEBUGGER debugger;
	/**
	 * Learner consulted during adaptive sections of the program.
	 */
	private final Learner learner;
	/**
	 * The executor call stack
	 */
	private final CallStack stack;
	/**
	 * The profiler. null if disabled
	 */
	private Profiler profiler = null;
	/**
	 * The timeout. 0 if disabled
	 */
	private long timeout = 0;

	/**
	 * Constructs a new interpreter.
	 *
	 * @param agentDf
	 *            to run
	 * @param debugger
	 *            used to debug the program
	 * @param learner
	 *            used to evaluate adaptive modules
	 */
	public GOALInterpreter(AgentDefinition agentDf, AgentRegistry<?> registry, DEBUGGER debugger, Learner learner) {
		this.agentDf = agentDf;
		this.registry = registry;
		this.debugger = debugger;
		this.learner = learner;
		this.stack = new CallStack();
	}

	/**
	 * @return the current run state of the interpreter
	 */
	public RunState getRunState() {
		return this.runState;
	}

	/*
	 * @return the current debugger used by the interpeter
	 */
	public DEBUGGER getDebugger() {
		return this.debugger;
	}

	@Override
	protected void initalizeController(Agent<? extends Controller> agent, Executor pool, long timeout)
			throws GOALLaunchFailureException {
		super.initalizeController(agent, pool, timeout);
		this.timeout = timeout;
	}

	protected void createRunstate() throws GOALLaunchFailureException {
		this.runState = new RunState(this, this.agent.getId(), this.agent.getEnvironment(), this.agent.getLogging(),
				this.agentDf, this.registry, this.learner, this.timeout);

		ExecutionEventListener listener = new ExecutionEventListener() {
			@Override
			public void goalEvent(Channel channel, Object associateObject, SourceInfo associateSource, String message,
					Object... args) {
				GOALInterpreter.this.debugger.breakpoint(channel, associateObject, associateSource, message, args);
			}
		};
		this.runState.getEventGenerator().addListener(listener);

		if (ProfilerPreferences.getProfiling()) {
			this.profiler = new Profiler(this.runState.getTimer());
			this.runState.getEventGenerator().addListener(this.profiler);
		}
	}

	@Override
	public void onReset() {
		try {
			this.debugger.reset();
			this.runState.reset();
		} catch (GOALLaunchFailureException e) {
			throw new GOALRuntimeErrorException(e); // FIXME
		}
	}

	@Override
	public void onTerminate() {
		this.debugger.kill();
		// hack to quickly show the profile results if enabled.
		if (this.profiler != null) {
			DataTable stats = this.profiler.getStats();

			StringBuilder profile = new StringBuilder();
			profile.append("profile for " + this.agent.getId()).append("\n");
			profile.append("--------------------").append("\n");

			List<ColumnType> cols = new ArrayList<>(6);
			cols.add(ProfileStatistic.Column.SOURCE);
			cols.add(ProfileStatistic.Column.CALLS);
			cols.add(ProfileStatistic.Column.TIME);
			cols.add(ProfileStatistic.Column.INFO);
			cols.add(ProfileStatistic.Column.THIS);
			cols.add(ProfileStatistic.Column.PARENT);
			profile.append(stats.header(cols, "\t")).append("\n");

			for (DataRow stat : stats.getData()) {
				if (ProfilerPreferences.isTypeSelected((InfoType) stat.column(ProfileStatistic.Column.TYPE))) {
					profile.append(stat.format(cols, "\t")).append("\n");
				}
			}
			profile.append("--------------------").append("\n");

			if (ProfilerPreferences.getProfilingToFile()) {
				try {
					DateFormat format = new SimpleDateFormat("yy-MM-dd_HH.mm.ss");
					String fname = this.agent.getId() + "_" + format.format(new Date()) + "_profile.txt";
					Files.write(Paths.get(LoggingPreferences.getLogDirectory() + File.separator + fname),
							profile.toString().getBytes());
				} catch (IOException e) {
					new Warning("failed writing profiler results to file for agent '" + this.agent + "'.", e);
				}
			} else {
				new InfoLog(profile.toString());
			}
		}
	}

	@Override
	protected Runnable getRunnable(final Executor pool, final Callable<Callable<?>> in) {
		return new Runnable() {
			@SuppressWarnings("unchecked")
			@Override
			public void run() {
				try {
					Callable<Callable<?>> call = in;
					if (call == null) {
						// Create the runstate, and inform the timer that thread is started.
						createRunstate();
						GOALInterpreter.this.runState.getTimer().get();
						// Initialization...
						if (!(GOALInterpreter.this.debugger instanceof NOPDebugger)) {
							GOALInterpreter.this.runState.getEventGenerator().event(Channel.REASONING_CYCLE_SEPARATOR,
									0, null, "started agent.");
						} else {
							// Let user know that agent has been started in case
							// NOPDebugger is used to allow the user to
							// experience some minimal progress.
							new InfoLog("agent '" + GOALInterpreter.this.agent.getId() + "' has been started.");
						}
						// Start the first cycle.
						GOALInterpreter.this.runState.startCycle(false);
						// Add the main module to the execution stack.
						Module main = GOALInterpreter.this.runState.getMainModule();
						ModuleExecutor mainExec = ModuleExecutor.getModuleExecutor(GOALInterpreter.this.stack,
								GOALInterpreter.this.runState, main, main.getKRInterface().getSubstitution(null),
								RuleEvaluationOrder.LINEAR);
						// mainExec.setSource(UseCase.MAIN);
						GOALInterpreter.this.stack.push(mainExec);
						// If we have an event module, AND there are some events
						// to proces, add it to the stack as well,
						// i.e. execute it before the main module.
						Module event = GOALInterpreter.this.runState.getEventModule();
						if (event != null) {
							ModuleExecutor eventExec = ModuleExecutor.getModuleExecutor(GOALInterpreter.this.stack,
									GOALInterpreter.this.runState, event, event.getKRInterface().getSubstitution(null),
									RuleEvaluationOrder.LINEARALL);
							// eventExec.setSource(UseCase.EVENT);
							GOALInterpreter.this.stack.push(eventExec);
						}
						// If we have an init module, add it to the stack as
						// well, i.e. execute it once before the other modules.
						Module init = GOALInterpreter.this.runState.getInitModule();
						if (init != null) {
							ModuleExecutor initExec = ModuleExecutor.getModuleExecutor(GOALInterpreter.this.stack,
									GOALInterpreter.this.runState, init, init.getKRInterface().getSubstitution(null),
									RuleEvaluationOrder.LINEARALL);
							// initExec.setSource(UseCase.INIT);
							GOALInterpreter.this.stack.push(initExec);
						}
						// Make executing the first cycle the initial call.
						call = executeCycle(GOALInterpreter.this.runState.getRoundCounter());
					}
					Callable<Callable<?>> out = null;
					if (call != null) {
						// Run the current task
						out = (Callable<Callable<?>>) call.call();
					}
					if (out != null && isRunning()) {
						// Submit the next task (when any)
						pool.execute(getRunnable(pool, out));
					} else {
						// Clean-up (terminate/dispose)
						GOALInterpreter.this.learner.terminate(GOALInterpreter.this.runState.getMentalState(),
								GOALInterpreter.this.runState.getReward());
						setTerminated();
						new InfoLog("agent '" + GOALInterpreter.this.agent.getId() + "' terminated successfully.");
					}
				} catch (final Exception e) { // Thread failure handling
					GOALInterpreter.this.throwable = e;
					if (e instanceof DebuggerKilledException) {
						// "normal" forced termination by the debugger.
						new InfoLog("agent '" + GOALInterpreter.this.agent.getId() + "' was killed externally.", e);
					} else {
						// something went wrong
						new Warning("agent '" + GOALInterpreter.this.agent.getId() + "' was forcefully terminated.", e);
					}
					try {
						setTerminated();
					} catch (final InterruptedException ie) {
						new Warning("unable to properly terminate agent '" + GOALInterpreter.this.agent.getId() + "'.",
								ie);
					}
				}
			}
		};
	}

	/**
	 * Uses the current {@link CallStack} for execution, i.e. popping (and thus
	 * executing) {@link StackExecutor}s from it, but only as long as we are in the
	 * indicated cycle. If we're not, and the stack is not empty (yet), a
	 * {@link Callable} will be returned that can be used to execute the next cycle
	 * (i.e. by feeding it into the threadpool).
	 *
	 * @param cycle
	 *            the cycle to execute
	 * @return
	 * @throws GOALActionFailedException
	 */
	private Callable<Callable<?>> executeCycle(final int cycle) throws GOALActionFailedException {
		while (this.stack.canExecute() && this.runState.getRoundCounter() == cycle) {
			this.stack.pop();
			this.stack.getPopped().getResult();
		}
		this.runState.getTimer().leaveThread();

		if (this.stack.canExecute()) {
			return new Callable<Callable<?>>() {
				@Override
				public Callable<?> call() throws Exception {
					return executeCycle(cycle + 1);
				}
			};
		} else {
			return null;
		}
	}

	/**
	 * @see {@link CallStack#getIndex()}
	 */
	public int getStackIndex() {
		return this.stack.getIndex();
	}

	/**
	 * @return the program ran by the interpreter
	 */
	public AgentDefinition getProgram() {
		return this.agentDf;
	}

	/**
	 * Execute actions manually.
	 *
	 * @param actions
	 *            The actions to be executed.
	 * @throws GOALActionFailedException
	 */
	public void doPerformAction(ActionCombo actions) throws GOALActionFailedException {
		for (Action<?> action : actions.getActions()) {
			boolean keeprunning = (action instanceof MentalAction) && (this.debugger instanceof SteppingDebugger);
			if (keeprunning) {
				((SteppingDebugger) this.debugger).setKeepRunning(true);
			}
			this.runState.doPerformAction(action);
			if (keeprunning) {
				((SteppingDebugger) this.debugger).setKeepRunning(false);
			}
		}
	}

	@Override
	public void dispose() {
		this.debugger.dispose();

		try {
			this.runState.dispose();
		} catch (MSTDatabaseException | MSTQueryException e) {
			throw new GOALRuntimeErrorException(e);
		}

		// agent is disposed by the AgentService, which is in turn disposed by
		// the RuntimeManager, so we don't need to do that here
	}

}
