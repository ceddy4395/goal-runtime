/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package goal.core.runtime.service.agent;

import languageTools.program.agent.Module;
import languageTools.program.agent.actions.Action;
import languageTools.program.agent.actions.ExitModuleAction;
import languageTools.program.agent.actions.ModuleCallAction;
import languageTools.program.agent.rules.Rule;

/**
 * Container for the results obtained by executing actions that is passed on to
 * {@link Module}s, {@link RuleSet}s, and {@link Rule}s.
 *
 * See, e.g.: {@link Module#run(goal.core.agent.Agent)} and
 * {@link Rule#run(goal.core.agent.AgentInt)}.
 */
public class Result {
	/**
	 * Flag that keeps track of whether last call to {@link #Result(Action)}
	 * reported that action was performed, i.e., Action was not {@code null}.
	 */
	private boolean justPerformedAction = false;
	/**
	 * Flag that keeps track of whether, when justPerformedAction is true, the
	 * action that was performed is a 'real' action (i.e. not a module call).
	 */
	private boolean actionWasReal = false;
	/**
	 * Indicates whether an exit module action has been performed.
	 */
	private boolean moduleIsTerminated = false;

	/**
	 * Creates default result.
	 */
	public Result() {
	}

	/**
	 * Add action to the result so far. An action counts as a 'real' action and
	 * is added to the list of executed actions if it is an external
	 * UserSpecAction.
	 *
	 * @param action
	 *            The result of executing an action that is to be added to this
	 *            result, or, {@code null} if no action was performed.
	 */
	public Result(Action<?> action) {
		this.justPerformedAction = (action != null);
		if (action instanceof ExitModuleAction) {
			this.moduleIsTerminated = true;
		} else if (!(action instanceof ModuleCallAction)) {
			this.actionWasReal = true;
		}
	}

	/**
	 * Checks if any action has just been performed, i.e., last call to
	 * {@link #add(Action)} reported that action was performed and Action was
	 * not {@code null}.
	 *
	 * @return {@code true} if any action has been performed; {@code false}
	 *         otherwise.
	 */
	public boolean justPerformedAction() {
		return this.justPerformedAction;
	}

	/**
	 * Checks if any 'real' action has just been performed, i.e., last call to
	 * {@link #add(Action)} reported that action was performed and Action was
	 * not {@code null}, and Action was not a {@link ModuleCallAction}.
	 *
	 * @return {@code true} if any action has been performed; {@code false}
	 *         otherwise.
	 */
	public boolean justPerformedRealAction() {
		return this.justPerformedAction && this.actionWasReal;
	}

	/**
	 * Set flag whether the current module should be terminated.
	 *
	 * @param terminated
	 *            The value for the module terminated flag.
	 */
	public void setModuleTerminated(boolean terminated) {
		this.moduleIsTerminated = terminated;
	}

	/**
	 * check if module was terminated
	 *
	 * @return {@code true} if module was terminated.
	 */
	public boolean isModuleTerminated() {
		return this.moduleIsTerminated;
	}

	/**
	 * Merges a new result with this {@link Result}.
	 *
	 * @param result
	 *            The result to be merged with this one.
	 */
	public void merge(Result result) {
		this.justPerformedAction |= result.justPerformedAction();
		this.actionWasReal |= result.justPerformedRealAction();
		this.moduleIsTerminated |= result.isModuleTerminated();
	}
}