package goal.tools.profiler;

import java.util.LinkedHashMap;
import java.util.Map;

import goal.util.datatable.ColumnType;
import goal.util.datatable.DataRow;
import krTools.parser.SourceInfo;

/**
 * {@link Statistic} with additional information for a profile.
 */
public class ProfileStatistic extends Statistic {

	/**
	 * {@link ColumnType}s to support table conversion.
	 *
	 */
	public static enum Column implements ColumnType {
		SOURCE("source"), CALLS("number of calls"), TIME("total time (s)"), INFO("source info"), TYPE("type"), THIS(
				"this"), PARENT("parent");

		private String description;

		Column(String descr) {
			this.description = descr;
		}

		@Override
		public String getDescription() {
			return this.description;
		}
	}

	private final String description;
	private SourceInfo sourceInfo;
	private InfoType type;
	private ProfileStatistic parent;

	/**
	 * create new statistic. args will be evaluated into the desription only
	 * when toString is called.
	 *
	 * @param info
	 *            the {@link SourceInfo} of the object. Can be null.
	 * @param desc
	 * @param args
	 */
	ProfileStatistic(SourceInfo info, InfoType type, String desc, ProfileStatistic parent) {
		this.sourceInfo = info;
		this.type = type;
		this.description = desc;
		this.parent = parent;
	}

	public SourceInfo getSourceInfo() {
		return this.sourceInfo;
	}

	/**
	 * @return the current statistics as DataRow.
	 */
	public DataRow getData() {
		Map<ColumnType, Object> data = new LinkedHashMap<>();
		data.put(Column.SOURCE, this.description);
		data.put(Column.CALLS, getTotalNumber());
		data.put(Column.TIME, getTotalTime() / 1000000000.);
		data.put(Column.INFO, this.sourceInfo);
		data.put(Column.TYPE, this.type);
		data.put(Column.THIS, this);
		data.put(Column.PARENT, this.parent);

		return new DataRow(data);
	}
}
