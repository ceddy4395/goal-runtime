/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package goal.tools.eclipse;

import java.util.List;
import java.util.Set;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

import events.NoEventGenerator;
import goal.core.agent.Agent;
import goal.core.agent.GOALInterpreter;
import goal.tools.errorhandling.exceptions.GOALActionFailedException;
import goal.tools.errorhandling.exceptions.GOALException;
import goal.tools.errorhandling.exceptions.GOALUserError;
import krTools.exceptions.ParserException;
import krTools.language.Substitution;
import krTools.language.Term;
import languageTools.analyzer.module.ModuleValidator;
import languageTools.errors.Message;
import languageTools.errors.module.ModuleErrorStrategy;
import languageTools.parser.GOALLexer;
import languageTools.parser.MOD2GParser;
import languageTools.program.actionspec.UserSpecAction;
import languageTools.program.agent.Module;
import languageTools.program.agent.Module.FocusMethod;
import languageTools.program.agent.actions.Action;
import languageTools.program.agent.actions.ActionCombo;
import languageTools.program.agent.actions.ExitModuleAction;
import languageTools.program.agent.actions.ModuleCallAction;
import languageTools.program.agent.actions.UserSpecOrModuleCall;
import languageTools.program.agent.msc.Macro;
import languageTools.program.agent.msc.MentalFormula;
import languageTools.program.agent.msc.MentalStateCondition;
import languageTools.program.mas.AgentDefinition;
import mentalState.MentalStateWithEvents;
import mentalState.error.MSTDatabaseException;
import mentalState.error.MSTQueryException;
import mentalState.executors.MentalStateConditionExecutor;

public class QueryTool {
	private final Agent<? extends GOALInterpreter<?>> agent;

	public QueryTool(final Agent<? extends GOALInterpreter<?>> agent) {
		this.agent = agent;
	}

	public String doquery(String userEnteredQuery) throws GOALUserError {
		MentalStateCondition msc;
		try {
			msc = parseMSC(userEnteredQuery);
		} catch (GOALException | ParserException e) {
			throw new GOALUserError(e.getMessage(), e.getCause());
		}
		// Perform query: get the agent's mental state and evaluate the query.
		MentalStateWithEvents mentalState = this.agent.getController().getRunState().getMentalState();
		try {
			// use a dummy debugger
			Substitution empty = mentalState.getOwner().getKRInterface().getSubstitution(null);
			Set<Substitution> substitutions = new MentalStateConditionExecutor(msc, empty)
					.evaluate(mentalState, FocusMethod.NONE, new NoEventGenerator()).getAnswers();
			String resulttext = "";
			if (substitutions.isEmpty()) {
				resulttext = "No solutions";
			} else {
				for (Substitution s : substitutions) {
					resulttext = resulttext + s + "\n";
				}
			}
			return resulttext;
		} catch (MSTQueryException | MSTDatabaseException e) {
			throw new GOALUserError(e.getMessage(), e.getCause());
		}
	}

	public String doaction(String userEnteredAction) throws GOALUserError {
		MentalStateWithEvents mentalState = this.agent.getController().getRunState().getMentalState();
		if (mentalState == null) {
			throw new GOALUserError("agent '" + this.agent.getId() + "' has not yet initialized its databases");
		}
		try {
			ActionCombo action = parseAction(userEnteredAction);
			this.agent.getController().doPerformAction(action);
			return "Executed " + action;
		} catch (ParserException pe) {
			throw new GOALUserError(pe.getMessage(), pe.getCause());
		} catch (GOALActionFailedException | GOALUserError e) {
			return e.getMessage();
		}
	}

	/**
	 * Creates an embedded module parser that can parse the given string.
	 *
	 * @param pString
	 *            is the string to be parsed.
	 * @return a MOD2GParser.
	 */
	private MOD2GParser prepareModuleParser(ModuleValidator validator, String pString) {
		CharStream charstream = CharStreams.fromString(pString, validator.getFilename());
		GOALLexer lexer = new GOALLexer(charstream);
		lexer.removeErrorListeners();
		lexer.addErrorListener(validator);
		MOD2GParser parser = new MOD2GParser(new CommonTokenStream(lexer));
		parser.setErrorHandler(new ModuleErrorStrategy());
		parser.removeErrorListeners();
		parser.addErrorListener(validator);
		return parser;
	}

	/**
	 * Parse a string to a {@link MentalStateCondition}.
	 *
	 * @param mentalStateCondition
	 *            Input string that should represent a mental state condition.
	 * @return The mental state condition that resulted from parsing the input
	 *         string.
	 * @throws GOALException
	 *             When the parser throws a RecognitionException, which should
	 *             have been buffered and ignored.
	 * @throws ParserException
	 */
	public MentalStateCondition parseMSC(String mentalStateCondition) throws GOALException, ParserException {
		// Try to parse the MSC.
		AgentDefinition agent = this.agent.getController().getProgram();
		ModuleValidator sub = new ModuleValidator("query-condition", agent.getRegistry());
		Module temp = new Module(agent.getRegistry(), null);
		temp.setKRInterface(agent.getKRInterface());
		sub.overrideProgram(temp);

		MOD2GParser parser = prepareModuleParser(sub, mentalStateCondition);
		MentalStateCondition msc = sub.visitMsc(parser.msc());
		checkParserErrors(sub, mentalStateCondition, "a mental state condition");

		// Check for macros, which cannot be resolved here because the proper
		// context is lacking.
		if (msc != null) {
			for (MentalFormula formula : msc.getSubFormulas()) {
				if (formula instanceof Macro) {
					throw new ParserException("cannot use macros here.", formula.getSourceInfo());
				}
			}
		}
		return msc;
	}

	/**
	 * check if any error has occurred. (only print lexer errors when there are
	 * no parser errors) Aggregate the error messages into a string, such that
	 * it can be put into the message of an exception to be thrown. The caller
	 * prints that message to the query console.
	 *
	 * @param validator
	 *            is the GOALWalker used to parse the string
	 * @param query
	 *            is the string that was fed to the GOALParser and that failed.
	 * @pparam desc is a string describing what was attempted to parse. Used for
	 *         error message generation if the parse failed.
	 * @throws ParserException
	 */
	private void checkParserErrors(ModuleValidator validator, String query, String desc) throws ParserException {
		Set<Message> errors = validator.getErrors();
		errors.addAll(validator.getSyntaxErrors());
		String errMessage = "";
		if (!errors.isEmpty()) {
			for (Message err : errors) {
				errMessage += err.toString() + "\n";
			}
		}

		// if any error has occurred, throw a ParserException
		if (!errMessage.isEmpty()) {
			throw new ParserException("'" + query + "' failed to parse as " + desc + ";\n" + errMessage,
					validator.getSource());
		}
	}

	/**
	 * Parse string as a mental action.
	 */
	@SuppressWarnings("unchecked")
	private ActionCombo parseAction(String action) throws ParserException, GOALUserError {
		// Try to parse the action
		AgentDefinition agent = this.agent.getController().getProgram();
		ModuleValidator sub = new ModuleValidator("query-action", agent.getRegistry());
		Module temp = new Module(agent.getRegistry(), null);
		temp.setKRInterface(agent.getKRInterface());
		sub.overrideProgram(temp);

		MOD2GParser parser = prepareModuleParser(sub, action);
		ActionCombo combo = sub.visitActioncombo(parser.actioncombo());
		checkParserErrors(sub, action, "a combination of actions");

		// Module calls are not allowed in the query tool, so in case the action
		// is not a mental action we will assume it is a user-specified action
		// that can be send to the environment.
		ActionCombo returned = new ActionCombo(null);
		if (combo != null) {
			for (Action<?> act : combo.getActions()) {
				if (act instanceof UserSpecOrModuleCall) {
					returned.addAction(new UserSpecAction(act.getName(), (List<Term>) act.getParameters(), true, null,
							null, null, null));
				} else if (!(act instanceof ExitModuleAction || act instanceof ModuleCallAction)) {
					returned.addAction(act);
				} else {
					throw new GOALUserError("Cannot call module actions here.");
				}
			}
		}
		return returned;
	}
}
