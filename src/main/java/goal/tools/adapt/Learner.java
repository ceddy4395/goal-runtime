/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package goal.tools.adapt;

import java.util.List;

import goal.core.executors.stack.ActionComboStackExecutor;
import goal.tools.errorhandling.exceptions.GOALRunFailedException;
import mentalState.MentalState;
import mentalState.MentalStateWithEvents;

/**
 * To make decisions in adaptive sections of the GOAL program the interpreter
 * consults a Learner. A Learner can make decisions based on past decisions and
 * rewards received from these decisions.
 *
 * <h1>Overview of Learning</h1>
 * <p>
 * The learning is based on Reinforcement Learning.
 *
 * The basic reinforcement learning model consists of:
 * <ol>
 * <li>a set of environment states S (the beliefs and goals; the current
 * module?);
 * <li>a set of actions A (the actions in the rules);
 * <li>rules of transitioning between states (the rules);
 * <li>rules that determine the scalar immediate reward of a transition (the
 * environment reward indicator or an estimate based on the number of remaining
 * goals )
 * <li>rules that describe what the agent observes (beliefs and goals. Percepts
 * and messages are ignored)
 * </ol>
 *
 * At each time t, the agent receives an observation o<sub>t</sub> , which
 * includes the reward r<sub>t</sub>. It then chooses an action a<sub>t</sub>
 * from the set of actions available, which is subsequently sent to the
 * environment. The environment moves to a new state s<sub>t+1</sub> and the
 * reward r<sub>t+1</sub> associated with the transition ( s<sub>t</sub> ,
 * a<sub>t</sub> , s<sub>t+1</sub> ) is determined. The goal of a reinforcement
 * learning agent is to collect as much reward as possible.
 * <p>
 * An example. Say the agent has just started and moved a block in blocksworld.
 * It should receive a reward for it. The reward in this case is likely 0,
 * unless it reached the desired blocks configurable with that last move in
 * which case it gets a reward of 1. So while the agent did not receive a
 * meaningful reward until the end, it did in fact "learn" at every step. This
 * becomes more evident over many runs as the "value" of each state+action pair
 * starts to become non-zero, and then the agent starts to make more meaningful
 * decisions about which action to take in a given state (based on which action
 * has the most known value so far in that state).
 * </p>
 * <h1>Rewarding an action</h1>
 * <p>
 * Determining the reward is the responsibility of the users of this module. But
 * we discuss it briefly. Not all environments support the notion of a reward.
 * If the environment has a reward value, that value can be used right away.
 * Otherwise, reward can be estimated from the number of remaining goals.
 * </p>
 * <h1>Saving the learned information</h1>
 * <p>
 * When the agent is died and {@link Learner#terminate(MentalStateWithEvents,
 * Double)()} is called, The learner then updates a <code>.lrn</code> file, a
 * <code>.adaptive.out</code> and a <code>.lrn.txt</code> file. the
 * <code>.lrn</code> file holds the saved learning from each run and should get
 * updated on each run. So at any time, if you killed the agent, you would have
 * saved the learning so far. We also allowed for the agent to start up with the
 * specified <code>.lrn</code> file (i.e., with prior learning). The
 * <code>.lrn.txt</code> file is more for users (students) so that they can get
 * some feedback on what was being learned (when coding, debugging). This file
 * could be shared/compared between students. The <code>.adaptive.out</code> is
 * the log of all states and actions and rewards from memory.
 */
public interface Learner {

	/**
	 * Selects an action from the list of options. The Learner can make this
	 * choice based on the current module, mental state and prior experiences.
	 *
	 * @param module
	 *            the current module.
	 * @param ms
	 *            the current mental state.
	 * @param options
	 *            from which the learner can choose an action executor.
	 * @return the selected action
	 */
	public abstract ActionComboStackExecutor act(String module, MentalStateWithEvents ms,
			List<ActionComboStackExecutor> options);

	/**
	 * Update the reward based on the last action.
	 *
	 * @param module
	 *            the current module
	 * @param ms
	 *            the current mental state
	 * @param reward
	 *            the reward for executing the last action selected by
	 *            {@link #act(String, MentalState, List)}.
	 */

	public abstract void update(String module, MentalStateWithEvents ms, double reward);

	/**
	 * Terminate all learning.
	 *
	 * @param ms
	 * @param envReward
	 * @throws GOALRunFailedException
	 */
	public abstract void terminate(MentalStateWithEvents ms, Double envReward) throws GOALRunFailedException;

}